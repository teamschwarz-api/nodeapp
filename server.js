'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
// const HOST = 'localhost'; // for local testing

// App
const app = express();

app.get('/', (req, res) => {
  let responseMessage = 'Automate all the things!';
  let mils = Date.now();

  res.send(JSON.stringify({
    message: responseMessage,
    timestamp: mils
  }) );

});

app.listen(PORT, HOST);
console.log(`12.06: Running on http://${HOST}:${PORT}`);
