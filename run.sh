echo -e "\n\n:::::::::::::::::::::::\n"
IMAGE_ID=`docker build . -q -t teamschwarz/nodeapp`
echo "$IMAGE_ID built"
CONTAINER_ID=`docker run -p 49160:8080 -d teamschwarz/nodeapp`
echo -e "$CONTAINER_ID started\n\nLogs:"
docker logs $CONTAINER_ID
echo -e "\n\n"
curl -i localhost:49160
echo "stopping"
docker stop $CONTAINER_ID
echo "removing"
docker rmi -f $IMAGE_ID
