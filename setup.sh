#!/bin/bash
#############################################################################
#
#   this script setup necessary things on the client side
#   
#############################################################################
clear
START_TIME=`date +%H":"%M":"%S`

choco install -y terraform
choco install aws-iam-authenticator
choco install kubernetes-cli
choco install wget
choco install -y --force nodejs
choco install docker-desktop
choco install postman

STOP_TIME=`date +%H":"%M":"%S`
echo ""
echo "START_TIME: "$START_TIME
echo "STOP_TIME:  "$STOP_TIME
echo ""
