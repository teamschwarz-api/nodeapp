pm.test("Body matches string", function () {
    pm.expect(pm.response.text()).to.include("Automate all the things!");
});

pm.test("Time Stamp Probably Right", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData.timestamp).to.be.above(1637688414206);
});