**Setup**

For Windows the following commands will setup everything needed. Manually install choclatey then run setup.sh which will:

choco install -y terraform

choco install aws-iam-authenticator

choco install kubernetes-cli

choco install wget

choco install -y --force nodejs

choco install docker-desktop #only required for local testing

choco install postman        #only required for local testing

---

AWS and kubectl need to be setup to access your account.

For additional information and a diagram that shows what has been developed, please see the demo deck located here : https://teamschwarz.atlassian.net/wiki/spaces/DAAS/pages/786433/Demo+Deck 

---

## Infrastructure

git clone git@bitbucket.org:teamschwarz-api/eksinfrastructure_demo.git

cd into the folder, and run deploy.sh

1. Terraform runs and creates the K8s cluster, VPC, permissions, etc
2. kubectl creates pod and service
3. Simple script echos out the pod and service that verify your "instance" is up and running

This takes about 15 minutes.

To destroy the environment run destroy.sh from the base folder.

hint: To see what will be created type terraform plan | more

---

## Application (This repo)

The application is server.js. Edit it in your favourite IDE and then commit to master (or commit in feature branch, and merge via PR to master). The pipeline will automatically kick off.

The pipeline dockerizes the node.js app and places it in Dockerhub.

There is nothing to destroy for this.

---



